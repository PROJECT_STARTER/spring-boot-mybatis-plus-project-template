package cn.com.chinarecrm.template.genarator;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.nutz.lang.Lang;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.FileOutConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.IFileCreate;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.TemplateConfig;
import com.baomidou.mybatisplus.generator.config.builder.ConfigBuilder;
import com.baomidou.mybatisplus.generator.config.po.TableFill;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.FileType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.BeetlTemplateEngine;

public class MysqlGenerator {

	public static String[] prefixes() {
		List<String> pool = Lang.array2list("abcdefghijklmnopqrstuvwxyz".toCharArray(), char.class).stream()
				.map(c -> c + "").collect(Collectors.toList());
		List<String> prefixes = pool.stream().map(item -> item + "_").collect(Collectors.toList());
		pool.stream().forEach(item -> {
			pool.stream().forEach(item1 -> {
				prefixes.add(String.format("%s%s_", item, item1));
			});
		});
		return Lang.collection2array(prefixes);
	}

	/**
	 * RUN THIS
	 */
	public static void main(String[] args) {
		// 代码生成器
		AutoGenerator autoGenerator = new AutoGenerator();

		// 全局配置
		GlobalConfig globalConfig = new GlobalConfig();
		String projectPath = System.getProperty("user.dir");
		globalConfig.setOutputDir(projectPath + "/src/main/java");
		globalConfig.setAuthor("chinarecrm");
		globalConfig.setOpen(false);
		globalConfig.setFileOverride(true);
		globalConfig.setActiveRecord(false);
		globalConfig.setSwagger2(true);
		globalConfig.setServiceName("%sService").setBaseResultMap(true).setBaseColumnList(true).setEntityName("%s")
				.setMapperName("%sMapper").setServiceImplName("%sServiceImpl").setControllerName("%sController")
				.setIdType(IdType.AUTO).setOpen(false);

		autoGenerator.setGlobalConfig(globalConfig);

		// 数据源配置
		DataSourceConfig dsc = new DataSourceConfig();
		dsc.setUrl(
				"jdbc:mysql://www.emasapple.cn:3306/thunder?useUnicode=true&serverTimezone=GMT&useSSL=false&characterEncoding=utf8");
		// dsc.setSchemaName("public");
		dsc.setDriverName("com.mysql.cj.jdbc.Driver");
		dsc.setUsername("root");
		dsc.setPassword("G00dl^ck");
		autoGenerator.setDataSource(dsc);

		// 包配置
		PackageConfig packageConfig = new PackageConfig();
		packageConfig.setController("controller");
		packageConfig.setService("service");
		packageConfig.setMapper("mapper");
		packageConfig.setEntity("entity");
		packageConfig.setParent("cn.com.chinarecrm");
		packageConfig.setModuleName("template");
		autoGenerator.setPackageInfo(packageConfig);

		// 自定义配置
		InjectionConfig injectionConfig = new InjectionConfig() {
			@Override
			public void initMap() {
				// TODO 数据初始化
			}
		};

		String templatePath = "/templates/mapper.xml.btl";
		List<FileOutConfig> focList = new ArrayList<>();
		// 自定义配置会被优先输出
		focList.add(new FileOutConfig(templatePath) {
			@Override
			public String outputFile(TableInfo tableInfo) {
				// 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
				return projectPath + "/src/main/resources/mapper/" + tableInfo.getEntityName() + "Mapper"
						+ StringPool.DOT_XML;
			}
		});
		injectionConfig.setFileCreate(new IFileCreate() {
			@Override
			public boolean isCreate(ConfigBuilder configBuilder, FileType fileType, String filePath) {
				// 判断自定义文件夹是否需要创建,这里调用默认的方法
				checkDir(filePath);
				// 对于已存在的文件，只需重复生成 entity 和 mapper.xml
				File file = new File(filePath);
				boolean exist = file.exists();
				return !exist || FileType.ENTITY == fileType;
			}
		});
		injectionConfig.setFileOutConfigList(focList);
		autoGenerator.setCfg(injectionConfig);

		// 配置模板
		TemplateConfig templateConfig = new TemplateConfig();
		// 关闭默认 xml 生成，调整生成 至 根目录
		templateConfig.setXml(null);
		templateConfig.setController("templates/mycontroller.java");
		templateConfig.setEntity("templates/myentity.java");
		autoGenerator.setTemplate(templateConfig);

		// 策略配置
		StrategyConfig strategy = new StrategyConfig();
		strategy.setNaming(NamingStrategy.underline_to_camel);
		strategy.setSuperEntityClass("cn.com.chinarecrm.template.entity.BaseEntity");
		strategy.setEntityLombokModel(true);
		strategy.setSuperEntityColumns("id", "created_time", "updated_time", "created_by", "updated_by");
		strategy.setLogicDeleteFieldName("status");
		strategy.setEntityTableFieldAnnotationEnable(true);
		strategy.setLogicDeleteFieldName("status");
		strategy.setControllerMappingHyphenStyle(false);
		strategy.setEntitySerialVersionUID(false);
		strategy.setTablePrefix("t_");
		strategy.setRestControllerStyle(true);
		// 所有单字符+双字符前缀
		strategy.setFieldPrefix(prefixes());

		strategy.setSuperMapperClass("com.baomidou.mybatisplus.core.mapper.BaseMapper");
		strategy.setControllerMappingHyphenStyle(true);
		strategy.setRestControllerStyle(true);

		List<TableFill> tableFills = new ArrayList<>();
		tableFills.add(new TableFill("create_time", FieldFill.INSERT));
		tableFills.add(new TableFill("update_time", FieldFill.UPDATE));
		strategy.setTableFillList(tableFills);

		autoGenerator.setStrategy(strategy);
		autoGenerator.setTemplateEngine(new BeetlTemplateEngine());
		autoGenerator.execute();
	}
}
