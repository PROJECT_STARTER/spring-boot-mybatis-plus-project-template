package cn.com.chinarecrm.template.config.jackson;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;

import org.nutz.lang.Lang;
import org.nutz.lang.Strings;
import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonStreamContext;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;

import cn.com.chinarecrm.template.enums.BaseEnum;

public class BaseEnumDeserializer extends JsonDeserializer<BaseEnum> {

    @Override
    public BaseEnum deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {

        JsonNode node = jp.getCodec().readTree(jp);
        String currentName = jp.currentName();
        Object currentValue = jp.getCurrentValue();
        Class findPropertyType = null;
        if (currentValue instanceof Collection) {

            JsonStreamContext parsingContext = jp.getParsingContext();

            JsonStreamContext parent = parsingContext.getParent();
            Object currentValue3 = parent.getCurrentValue();
            String currentName3 = parent.getCurrentName();
            try {
                Field listField = currentValue3.getClass().getDeclaredField(currentName3);
                ParameterizedType listGenericType = (ParameterizedType)listField.getGenericType();
                Type listActualTypeArguments = listGenericType.getActualTypeArguments()[0];
                findPropertyType = (Class)listActualTypeArguments;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            findPropertyType = BeanUtils.findPropertyType(currentName, currentValue.getClass());
        }
        if (findPropertyType == null) {
            throw Lang.makeThrow("数据格式异常");
        }
        String asText = null;
        if (node.getNodeType() == JsonNodeType.STRING) {
            asText = node.asText();
        } else {
            asText = node.get("code").asText();
        }
        BaseEnum valueOf = null;
        if (Strings.isNotBlank(asText)) {
            valueOf = BaseEnum.valueOf(asText, findPropertyType);
        }
        return valueOf;
    }

}