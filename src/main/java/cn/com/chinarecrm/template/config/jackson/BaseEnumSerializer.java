package cn.com.chinarecrm.template.config.jackson;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import cn.com.chinarecrm.template.enums.BaseEnum;

public class BaseEnumSerializer extends JsonSerializer<BaseEnum> {

    @Override
    public void serialize(BaseEnum value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        Map<String, String> map = new HashMap<>();
        map.put("code", value.getCode());
        map.put("name", value.getName());
        gen.writeObject(map);
    }

}
