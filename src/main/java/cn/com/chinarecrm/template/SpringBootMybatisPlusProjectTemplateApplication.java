package cn.com.chinarecrm.template;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;

@SpringBootApplication
public class SpringBootMybatisPlusProjectTemplateApplication {

	public static void main(String[] args) {
		SpringApplication springApplication = new SpringApplication(SpringBootMybatisPlusProjectTemplateApplication.class);
        springApplication.addListeners(new ApplicationPidFileWriter());
        springApplication.run(args);
	}

}
