package cn.com.chinarecrm.template.mapper;

import cn.com.chinarecrm.template.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户角色关系 Mapper 接口
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
