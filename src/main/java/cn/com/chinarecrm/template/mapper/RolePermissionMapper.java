package cn.com.chinarecrm.template.mapper;

import cn.com.chinarecrm.template.entity.RolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色权限关系表 Mapper 接口
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

}
