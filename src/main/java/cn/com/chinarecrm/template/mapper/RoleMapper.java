package cn.com.chinarecrm.template.mapper;

import cn.com.chinarecrm.template.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
public interface RoleMapper extends BaseMapper<Role> {

}
