package cn.com.chinarecrm.template.mapper;

import cn.com.chinarecrm.template.entity.Group;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 码本分组 Mapper 接口
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
public interface GroupMapper extends BaseMapper<Group> {

}
