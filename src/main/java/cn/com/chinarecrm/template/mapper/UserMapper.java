package cn.com.chinarecrm.template.mapper;

import cn.com.chinarecrm.template.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户 Mapper 接口
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
public interface UserMapper extends BaseMapper<User> {

}
