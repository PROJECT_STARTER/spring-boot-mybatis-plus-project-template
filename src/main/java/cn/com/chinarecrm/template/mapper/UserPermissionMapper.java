package cn.com.chinarecrm.template.mapper;

import cn.com.chinarecrm.template.entity.UserPermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户权限关系 Mapper 接口
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
public interface UserPermissionMapper extends BaseMapper<UserPermission> {

}
