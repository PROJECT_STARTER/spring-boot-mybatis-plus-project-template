package cn.com.chinarecrm.template.mapper;

import cn.com.chinarecrm.template.entity.Action;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 功能动作 Mapper 接口
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
public interface ActionMapper extends BaseMapper<Action> {

}
