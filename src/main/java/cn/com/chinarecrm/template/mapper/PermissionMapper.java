package cn.com.chinarecrm.template.mapper;

import cn.com.chinarecrm.template.entity.Permission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 权限 Mapper 接口
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
public interface PermissionMapper extends BaseMapper<Permission> {

}
