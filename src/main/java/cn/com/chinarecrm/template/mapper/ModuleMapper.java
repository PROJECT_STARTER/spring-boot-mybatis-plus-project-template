package cn.com.chinarecrm.template.mapper;

import cn.com.chinarecrm.template.entity.Module;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 功能模块 Mapper 接口
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
public interface ModuleMapper extends BaseMapper<Module> {

}
