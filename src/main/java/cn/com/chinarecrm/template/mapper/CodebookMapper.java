package cn.com.chinarecrm.template.mapper;

import cn.com.chinarecrm.template.entity.Codebook;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 码本数据 Mapper 接口
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
public interface CodebookMapper extends BaseMapper<Codebook> {

}
