package cn.com.chinarecrm.template.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import club.zhcs.Result;
import cn.com.chinarecrm.template.entity.Permission;
import cn.com.chinarecrm.template.service.PermissionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
/**
 * <p>
 * 权限 前端控制器
 * </p>
 *
 * @author chinarecrm
 * @since 2021-01-11
 */
@RestController
 @Api(value = "Permission", tags = {"权限模块"})
public class PermissionController {

	
    @Autowired
    PermissionService permissionService;

    /**
     * 分页查询权限
     * 
     * @param page
     *            页码
     * @param size
     *            分页大小
     * @param key
     *            关键词
     * @return 权限分页数据
     */
    @GetMapping("permissions")
     @ApiOperation("分页查询权限")
      public Result<IPage<Permission>> permissions(@ApiParam(value = "页码", required = true) @RequestParam("page") long page,
                                           @ApiParam(value = "页面大小", required = true) @RequestParam("size") long size,
                                           @ApiParam("搜索关键词")  @RequestParam(name = "key", required = false, defaultValue = "") String key) {
        return Result.success(permissionService.page(new Page<Permission>(page, size)));
    }

    /**
     * 权限详情
     * 
     * @param id
     *            权限id
     * @return 权限
     */
    @GetMapping("permission/{id}")
    	@ApiOperation("权限详情")
		public Result<Permission> detail(@ApiParam(value = "权限id", required = true) @PathVariable("id") long id) {
        return Result.success(permissionService.getById(id));
    }

    /**
     * 添加或者更新权限
     * 
     * @param permission
     *            权限数据
     * @return 权限
     */
    @PutMapping("permission")
     @ApiOperation("增加/编辑权限")
      public Result<Permission> saveOrUpdate(@ApiParam("增加/编辑权限选项")@RequestBody Permission permission) {
        if (permission.getId() > 0) {
            return permissionService.update(permission, Wrappers.<Permission>lambdaUpdate().eq(Permission::getId, permission.getId())) ? Result.success(permission) : Result.fail("更新权限分组失败!");
        }
        return permissionService.save(permission) ? Result.success(permission) : Result.fail("保存权限失败!");
    }

    /**
     * 删除权限
     * 
     * @param id
     *            权限id
     * @return 是否删除成功
     */
    @DeleteMapping("permission/{id}")
     @ApiOperation("删除权限")
      public Result<Void> delete(@ApiParam(value = "权限id", required = true)@PathVariable("id") long id) {
        return permissionService.removeById(id) ? Result.success() : Result.fail("删除权限失败!");
    }

}
