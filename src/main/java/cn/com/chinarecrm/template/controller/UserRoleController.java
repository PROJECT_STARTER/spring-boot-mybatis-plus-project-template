package cn.com.chinarecrm.template.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import club.zhcs.Result;
import cn.com.chinarecrm.template.entity.UserRole;
import cn.com.chinarecrm.template.service.UserRoleService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
/**
 * <p>
 * 用户角色关系 前端控制器
 * </p>
 *
 * @author chinarecrm
 * @since 2021-01-11
 */
@RestController
 @Api(value = "UserRole", tags = {"用户角色关系模块"})
public class UserRoleController {

	
    @Autowired
    UserRoleService userRoleService;

    /**
     * 分页查询用户角色关系
     * 
     * @param page
     *            页码
     * @param size
     *            分页大小
     * @param key
     *            关键词
     * @return 用户角色关系分页数据
     */
    @GetMapping("user-roles")
     @ApiOperation("分页查询用户角色关系")
      public Result<IPage<UserRole>> userRoles(@ApiParam(value = "页码", required = true) @RequestParam("page") long page,
                                           @ApiParam(value = "页面大小", required = true) @RequestParam("size") long size,
                                           @ApiParam("搜索关键词")  @RequestParam(name = "key", required = false, defaultValue = "") String key) {
        return Result.success(userRoleService.page(new Page<UserRole>(page, size)));
    }

    /**
     * 用户角色关系详情
     * 
     * @param id
     *            用户角色关系id
     * @return 用户角色关系
     */
    @GetMapping("user-role/{id}")
    	@ApiOperation("用户角色关系详情")
		public Result<UserRole> detail(@ApiParam(value = "用户角色关系id", required = true) @PathVariable("id") long id) {
        return Result.success(userRoleService.getById(id));
    }

    /**
     * 添加或者更新用户角色关系
     * 
     * @param userRole
     *            用户角色关系数据
     * @return 用户角色关系
     */
    @PutMapping("user-role")
     @ApiOperation("增加/编辑用户角色关系")
      public Result<UserRole> saveOrUpdate(@ApiParam("增加/编辑用户角色关系选项")@RequestBody UserRole userRole) {
        if (userRole.getId() > 0) {
            return userRoleService.update(userRole, Wrappers.<UserRole>lambdaUpdate().eq(UserRole::getId, userRole.getId())) ? Result.success(userRole) : Result.fail("更新用户角色关系分组失败!");
        }
        return userRoleService.save(userRole) ? Result.success(userRole) : Result.fail("保存用户角色关系失败!");
    }

    /**
     * 删除用户角色关系
     * 
     * @param id
     *            用户角色关系id
     * @return 是否删除成功
     */
    @DeleteMapping("user-role/{id}")
     @ApiOperation("删除用户角色关系")
      public Result<Void> delete(@ApiParam(value = "用户角色关系id", required = true)@PathVariable("id") long id) {
        return userRoleService.removeById(id) ? Result.success() : Result.fail("删除用户角色关系失败!");
    }

}
