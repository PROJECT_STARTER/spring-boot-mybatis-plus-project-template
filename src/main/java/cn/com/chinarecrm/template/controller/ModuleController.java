package cn.com.chinarecrm.template.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import club.zhcs.Result;
import cn.com.chinarecrm.template.entity.Module;
import cn.com.chinarecrm.template.service.ModuleService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
/**
 * <p>
 * 功能模块 前端控制器
 * </p>
 *
 * @author chinarecrm
 * @since 2021-01-11
 */
@RestController
 @Api(value = "Module", tags = {"功能模块模块"})
public class ModuleController {

	
    @Autowired
    ModuleService moduleService;

    /**
     * 分页查询功能模块
     * 
     * @param page
     *            页码
     * @param size
     *            分页大小
     * @param key
     *            关键词
     * @return 功能模块分页数据
     */
    @GetMapping("modules")
     @ApiOperation("分页查询功能模块")
      public Result<IPage<Module>> modules(@ApiParam(value = "页码", required = true) @RequestParam("page") long page,
                                           @ApiParam(value = "页面大小", required = true) @RequestParam("size") long size,
                                           @ApiParam("搜索关键词")  @RequestParam(name = "key", required = false, defaultValue = "") String key) {
        return Result.success(moduleService.page(new Page<Module>(page, size)));
    }

    /**
     * 功能模块详情
     * 
     * @param id
     *            功能模块id
     * @return 功能模块
     */
    @GetMapping("module/{id}")
    	@ApiOperation("功能模块详情")
		public Result<Module> detail(@ApiParam(value = "功能模块id", required = true) @PathVariable("id") long id) {
        return Result.success(moduleService.getById(id));
    }

    /**
     * 添加或者更新功能模块
     * 
     * @param module
     *            功能模块数据
     * @return 功能模块
     */
    @PutMapping("module")
     @ApiOperation("增加/编辑功能模块")
      public Result<Module> saveOrUpdate(@ApiParam("增加/编辑功能模块选项")@RequestBody Module module) {
        if (module.getId() > 0) {
            return moduleService.update(module, Wrappers.<Module>lambdaUpdate().eq(Module::getId, module.getId())) ? Result.success(module) : Result.fail("更新功能模块分组失败!");
        }
        return moduleService.save(module) ? Result.success(module) : Result.fail("保存功能模块失败!");
    }

    /**
     * 删除功能模块
     * 
     * @param id
     *            功能模块id
     * @return 是否删除成功
     */
    @DeleteMapping("module/{id}")
     @ApiOperation("删除功能模块")
      public Result<Void> delete(@ApiParam(value = "功能模块id", required = true)@PathVariable("id") long id) {
        return moduleService.removeById(id) ? Result.success() : Result.fail("删除功能模块失败!");
    }

}
