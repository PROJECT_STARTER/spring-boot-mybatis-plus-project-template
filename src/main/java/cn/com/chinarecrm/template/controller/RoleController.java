package cn.com.chinarecrm.template.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import club.zhcs.Result;
import cn.com.chinarecrm.template.entity.Role;
import cn.com.chinarecrm.template.service.RoleService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
/**
 * <p>
 * 角色 前端控制器
 * </p>
 *
 * @author chinarecrm
 * @since 2021-01-11
 */
@RestController
 @Api(value = "Role", tags = {"角色模块"})
public class RoleController {

	
    @Autowired
    RoleService roleService;

    /**
     * 分页查询角色
     * 
     * @param page
     *            页码
     * @param size
     *            分页大小
     * @param key
     *            关键词
     * @return 角色分页数据
     */
    @GetMapping("roles")
     @ApiOperation("分页查询角色")
      public Result<IPage<Role>> roles(@ApiParam(value = "页码", required = true) @RequestParam("page") long page,
                                           @ApiParam(value = "页面大小", required = true) @RequestParam("size") long size,
                                           @ApiParam("搜索关键词")  @RequestParam(name = "key", required = false, defaultValue = "") String key) {
        return Result.success(roleService.page(new Page<Role>(page, size)));
    }

    /**
     * 角色详情
     * 
     * @param id
     *            角色id
     * @return 角色
     */
    @GetMapping("role/{id}")
    	@ApiOperation("角色详情")
		public Result<Role> detail(@ApiParam(value = "角色id", required = true) @PathVariable("id") long id) {
        return Result.success(roleService.getById(id));
    }

    /**
     * 添加或者更新角色
     * 
     * @param role
     *            角色数据
     * @return 角色
     */
    @PutMapping("role")
     @ApiOperation("增加/编辑角色")
      public Result<Role> saveOrUpdate(@ApiParam("增加/编辑角色选项")@RequestBody Role role) {
        if (role.getId() > 0) {
            return roleService.update(role, Wrappers.<Role>lambdaUpdate().eq(Role::getId, role.getId())) ? Result.success(role) : Result.fail("更新角色分组失败!");
        }
        return roleService.save(role) ? Result.success(role) : Result.fail("保存角色失败!");
    }

    /**
     * 删除角色
     * 
     * @param id
     *            角色id
     * @return 是否删除成功
     */
    @DeleteMapping("role/{id}")
     @ApiOperation("删除角色")
      public Result<Void> delete(@ApiParam(value = "角色id", required = true)@PathVariable("id") long id) {
        return roleService.removeById(id) ? Result.success() : Result.fail("删除角色失败!");
    }

}
