package cn.com.chinarecrm.template.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import club.zhcs.Result;
import cn.com.chinarecrm.template.entity.Group;
import cn.com.chinarecrm.template.service.GroupService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
/**
 * <p>
 * 码本分组 前端控制器
 * </p>
 *
 * @author chinarecrm
 * @since 2021-01-11
 */
@RestController
 @Api(value = "Group", tags = {"码本分组模块"})
public class GroupController {

	
    @Autowired
    GroupService groupService;

    /**
     * 分页查询码本分组
     * 
     * @param page
     *            页码
     * @param size
     *            分页大小
     * @param key
     *            关键词
     * @return 码本分组分页数据
     */
    @GetMapping("groups")
     @ApiOperation("分页查询码本分组")
      public Result<IPage<Group>> groups(@ApiParam(value = "页码", required = true) @RequestParam("page") long page,
                                           @ApiParam(value = "页面大小", required = true) @RequestParam("size") long size,
                                           @ApiParam("搜索关键词")  @RequestParam(name = "key", required = false, defaultValue = "") String key) {
        return Result.success(groupService.page(new Page<Group>(page, size)));
    }

    /**
     * 码本分组详情
     * 
     * @param id
     *            码本分组id
     * @return 码本分组
     */
    @GetMapping("group/{id}")
    	@ApiOperation("码本分组详情")
		public Result<Group> detail(@ApiParam(value = "码本分组id", required = true) @PathVariable("id") long id) {
        return Result.success(groupService.getById(id));
    }

    /**
     * 添加或者更新码本分组
     * 
     * @param group
     *            码本分组数据
     * @return 码本分组
     */
    @PutMapping("group")
     @ApiOperation("增加/编辑码本分组")
      public Result<Group> saveOrUpdate(@ApiParam("增加/编辑码本分组选项")@RequestBody Group group) {
        if (group.getId() > 0) {
            return groupService.update(group, Wrappers.<Group>lambdaUpdate().eq(Group::getId, group.getId())) ? Result.success(group) : Result.fail("更新码本分组分组失败!");
        }
        return groupService.save(group) ? Result.success(group) : Result.fail("保存码本分组失败!");
    }

    /**
     * 删除码本分组
     * 
     * @param id
     *            码本分组id
     * @return 是否删除成功
     */
    @DeleteMapping("group/{id}")
     @ApiOperation("删除码本分组")
      public Result<Void> delete(@ApiParam(value = "码本分组id", required = true)@PathVariable("id") long id) {
        return groupService.removeById(id) ? Result.success() : Result.fail("删除码本分组失败!");
    }

}
