package cn.com.chinarecrm.template.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import club.zhcs.Result;
import cn.com.chinarecrm.template.entity.User;
import cn.com.chinarecrm.template.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
/**
 * <p>
 * 用户 前端控制器
 * </p>
 *
 * @author chinarecrm
 * @since 2021-01-11
 */
@RestController
 @Api(value = "User", tags = {"用户模块"})
public class UserController {

	
    @Autowired
    UserService userService;

    /**
     * 分页查询用户
     * 
     * @param page
     *            页码
     * @param size
     *            分页大小
     * @param key
     *            关键词
     * @return 用户分页数据
     */
    @GetMapping("users")
     @ApiOperation("分页查询用户")
      public Result<IPage<User>> users(@ApiParam(value = "页码", required = true) @RequestParam("page") long page,
                                           @ApiParam(value = "页面大小", required = true) @RequestParam("size") long size,
                                           @ApiParam("搜索关键词")  @RequestParam(name = "key", required = false, defaultValue = "") String key) {
        return Result.success(userService.page(new Page<User>(page, size)));
    }

    /**
     * 用户详情
     * 
     * @param id
     *            用户id
     * @return 用户
     */
    @GetMapping("user/{id}")
    	@ApiOperation("用户详情")
		public Result<User> detail(@ApiParam(value = "用户id", required = true) @PathVariable("id") long id) {
        return Result.success(userService.getById(id));
    }

    /**
     * 添加或者更新用户
     * 
     * @param user
     *            用户数据
     * @return 用户
     */
    @PutMapping("user")
     @ApiOperation("增加/编辑用户")
      public Result<User> saveOrUpdate(@ApiParam("增加/编辑用户选项")@RequestBody User user) {
        if (user.getId() > 0) {
            return userService.update(user, Wrappers.<User>lambdaUpdate().eq(User::getId, user.getId())) ? Result.success(user) : Result.fail("更新用户分组失败!");
        }
        return userService.save(user) ? Result.success(user) : Result.fail("保存用户失败!");
    }

    /**
     * 删除用户
     * 
     * @param id
     *            用户id
     * @return 是否删除成功
     */
    @DeleteMapping("user/{id}")
     @ApiOperation("删除用户")
      public Result<Void> delete(@ApiParam(value = "用户id", required = true)@PathVariable("id") long id) {
        return userService.removeById(id) ? Result.success() : Result.fail("删除用户失败!");
    }

}
