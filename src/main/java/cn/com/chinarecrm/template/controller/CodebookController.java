package cn.com.chinarecrm.template.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import club.zhcs.Result;
import cn.com.chinarecrm.template.entity.Codebook;
import cn.com.chinarecrm.template.service.CodebookService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
/**
 * <p>
 * 码本数据 前端控制器
 * </p>
 *
 * @author chinarecrm
 * @since 2021-01-11
 */
@RestController
 @Api(value = "Codebook", tags = {"码本数据模块"})
public class CodebookController {

	
    @Autowired
    CodebookService codebookService;

    /**
     * 分页查询码本数据
     * 
     * @param page
     *            页码
     * @param size
     *            分页大小
     * @param key
     *            关键词
     * @return 码本数据分页数据
     */
    @GetMapping("codebooks")
     @ApiOperation("分页查询码本数据")
      public Result<IPage<Codebook>> codebooks(@ApiParam(value = "页码", required = true) @RequestParam("page") long page,
                                           @ApiParam(value = "页面大小", required = true) @RequestParam("size") long size,
                                           @ApiParam("搜索关键词")  @RequestParam(name = "key", required = false, defaultValue = "") String key) {
        return Result.success(codebookService.page(new Page<Codebook>(page, size)));
    }

    /**
     * 码本数据详情
     * 
     * @param id
     *            码本数据id
     * @return 码本数据
     */
    @GetMapping("codebook/{id}")
    	@ApiOperation("码本数据详情")
		public Result<Codebook> detail(@ApiParam(value = "码本数据id", required = true) @PathVariable("id") long id) {
        return Result.success(codebookService.getById(id));
    }

    /**
     * 添加或者更新码本数据
     * 
     * @param codebook
     *            码本数据数据
     * @return 码本数据
     */
    @PutMapping("codebook")
     @ApiOperation("增加/编辑码本数据")
      public Result<Codebook> saveOrUpdate(@ApiParam("增加/编辑码本数据选项")@RequestBody Codebook codebook) {
        if (codebook.getId() > 0) {
            return codebookService.update(codebook, Wrappers.<Codebook>lambdaUpdate().eq(Codebook::getId, codebook.getId())) ? Result.success(codebook) : Result.fail("更新码本数据分组失败!");
        }
        return codebookService.save(codebook) ? Result.success(codebook) : Result.fail("保存码本数据失败!");
    }

    /**
     * 删除码本数据
     * 
     * @param id
     *            码本数据id
     * @return 是否删除成功
     */
    @DeleteMapping("codebook/{id}")
     @ApiOperation("删除码本数据")
      public Result<Void> delete(@ApiParam(value = "码本数据id", required = true)@PathVariable("id") long id) {
        return codebookService.removeById(id) ? Result.success() : Result.fail("删除码本数据失败!");
    }

}
