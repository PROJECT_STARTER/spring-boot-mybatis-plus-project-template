package cn.com.chinarecrm.template.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import club.zhcs.Result;
import cn.com.chinarecrm.template.entity.Action;
import cn.com.chinarecrm.template.service.ActionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
/**
 * <p>
 * 功能动作 前端控制器
 * </p>
 *
 * @author chinarecrm
 * @since 2021-01-11
 */
@RestController
 @Api(value = "Action", tags = {"功能动作模块"})
public class ActionController {

	
    @Autowired
    ActionService actionService;

    /**
     * 分页查询功能动作
     * 
     * @param page
     *            页码
     * @param size
     *            分页大小
     * @param key
     *            关键词
     * @return 功能动作分页数据
     */
    @GetMapping("actions")
     @ApiOperation("分页查询功能动作")
      public Result<IPage<Action>> actions(@ApiParam(value = "页码", required = true) @RequestParam("page") long page,
                                           @ApiParam(value = "页面大小", required = true) @RequestParam("size") long size,
                                           @ApiParam("搜索关键词")  @RequestParam(name = "key", required = false, defaultValue = "") String key) {
        return Result.success(actionService.page(new Page<Action>(page, size)));
    }

    /**
     * 功能动作详情
     * 
     * @param id
     *            功能动作id
     * @return 功能动作
     */
    @GetMapping("action/{id}")
    	@ApiOperation("功能动作详情")
		public Result<Action> detail(@ApiParam(value = "功能动作id", required = true) @PathVariable("id") long id) {
        return Result.success(actionService.getById(id));
    }

    /**
     * 添加或者更新功能动作
     * 
     * @param action
     *            功能动作数据
     * @return 功能动作
     */
    @PutMapping("action")
     @ApiOperation("增加/编辑功能动作")
      public Result<Action> saveOrUpdate(@ApiParam("增加/编辑功能动作选项")@RequestBody Action action) {
        if (action.getId() > 0) {
            return actionService.update(action, Wrappers.<Action>lambdaUpdate().eq(Action::getId, action.getId())) ? Result.success(action) : Result.fail("更新功能动作分组失败!");
        }
        return actionService.save(action) ? Result.success(action) : Result.fail("保存功能动作失败!");
    }

    /**
     * 删除功能动作
     * 
     * @param id
     *            功能动作id
     * @return 是否删除成功
     */
    @DeleteMapping("action/{id}")
     @ApiOperation("删除功能动作")
      public Result<Void> delete(@ApiParam(value = "功能动作id", required = true)@PathVariable("id") long id) {
        return actionService.removeById(id) ? Result.success() : Result.fail("删除功能动作失败!");
    }

}
