package cn.com.chinarecrm.template.controller;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import club.zhcs.Result;
import club.zhcs.apm.APM;
import cn.com.chinarecrm.template.enums.BaseEnum;
import cn.com.chinarecrm.template.enums.CommonStatus;
import cn.com.chinarecrm.template.service.RoleService;

/**
 * 
 * @author kerbores(kerbores@gmail.com)
 *
 */
@RestController
@APM
public class TestController {

    @Autowired
    RoleService roleService;

    @GetMapping("date")
    public Result<Void> date(@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime dateTime) {
        System.err.println(dateTime);
        return Result.success();
    }

    @GetMapping("enums")
    public Result<BaseEnum[]> enResult() {
        return Result.success(CommonStatus.values());
    }
}
