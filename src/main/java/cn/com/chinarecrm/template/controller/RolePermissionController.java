package cn.com.chinarecrm.template.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import club.zhcs.Result;
import cn.com.chinarecrm.template.entity.RolePermission;
import cn.com.chinarecrm.template.service.RolePermissionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
/**
 * <p>
 * 角色权限关系表 前端控制器
 * </p>
 *
 * @author chinarecrm
 * @since 2021-01-11
 */
@RestController
 @Api(value = "RolePermission", tags = {"角色权限关系表模块"})
public class RolePermissionController {

	
    @Autowired
    RolePermissionService rolePermissionService;

    /**
     * 分页查询角色权限关系表
     * 
     * @param page
     *            页码
     * @param size
     *            分页大小
     * @param key
     *            关键词
     * @return 角色权限关系表分页数据
     */
    @GetMapping("role-permissions")
     @ApiOperation("分页查询角色权限关系表")
      public Result<IPage<RolePermission>> rolePermissions(@ApiParam(value = "页码", required = true) @RequestParam("page") long page,
                                           @ApiParam(value = "页面大小", required = true) @RequestParam("size") long size,
                                           @ApiParam("搜索关键词")  @RequestParam(name = "key", required = false, defaultValue = "") String key) {
        return Result.success(rolePermissionService.page(new Page<RolePermission>(page, size)));
    }

    /**
     * 角色权限关系表详情
     * 
     * @param id
     *            角色权限关系表id
     * @return 角色权限关系表
     */
    @GetMapping("role-permission/{id}")
    	@ApiOperation("角色权限关系表详情")
		public Result<RolePermission> detail(@ApiParam(value = "角色权限关系表id", required = true) @PathVariable("id") long id) {
        return Result.success(rolePermissionService.getById(id));
    }

    /**
     * 添加或者更新角色权限关系表
     * 
     * @param rolePermission
     *            角色权限关系表数据
     * @return 角色权限关系表
     */
    @PutMapping("role-permission")
     @ApiOperation("增加/编辑角色权限关系表")
      public Result<RolePermission> saveOrUpdate(@ApiParam("增加/编辑角色权限关系表选项")@RequestBody RolePermission rolePermission) {
        if (rolePermission.getId() > 0) {
            return rolePermissionService.update(rolePermission, Wrappers.<RolePermission>lambdaUpdate().eq(RolePermission::getId, rolePermission.getId())) ? Result.success(rolePermission) : Result.fail("更新角色权限关系表分组失败!");
        }
        return rolePermissionService.save(rolePermission) ? Result.success(rolePermission) : Result.fail("保存角色权限关系表失败!");
    }

    /**
     * 删除角色权限关系表
     * 
     * @param id
     *            角色权限关系表id
     * @return 是否删除成功
     */
    @DeleteMapping("role-permission/{id}")
     @ApiOperation("删除角色权限关系表")
      public Result<Void> delete(@ApiParam(value = "角色权限关系表id", required = true)@PathVariable("id") long id) {
        return rolePermissionService.removeById(id) ? Result.success() : Result.fail("删除角色权限关系表失败!");
    }

}
