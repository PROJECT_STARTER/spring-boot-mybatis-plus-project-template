package cn.com.chinarecrm.template.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import club.zhcs.Result;
import cn.com.chinarecrm.template.entity.UserPermission;
import cn.com.chinarecrm.template.service.UserPermissionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
/**
 * <p>
 * 用户权限关系 前端控制器
 * </p>
 *
 * @author chinarecrm
 * @since 2021-01-11
 */
@RestController
 @Api(value = "UserPermission", tags = {"用户权限关系模块"})
public class UserPermissionController {

	
    @Autowired
    UserPermissionService userPermissionService;

    /**
     * 分页查询用户权限关系
     * 
     * @param page
     *            页码
     * @param size
     *            分页大小
     * @param key
     *            关键词
     * @return 用户权限关系分页数据
     */
    @GetMapping("user-permissions")
     @ApiOperation("分页查询用户权限关系")
      public Result<IPage<UserPermission>> userPermissions(@ApiParam(value = "页码", required = true) @RequestParam("page") long page,
                                           @ApiParam(value = "页面大小", required = true) @RequestParam("size") long size,
                                           @ApiParam("搜索关键词")  @RequestParam(name = "key", required = false, defaultValue = "") String key) {
        return Result.success(userPermissionService.page(new Page<UserPermission>(page, size)));
    }

    /**
     * 用户权限关系详情
     * 
     * @param id
     *            用户权限关系id
     * @return 用户权限关系
     */
    @GetMapping("user-permission/{id}")
    	@ApiOperation("用户权限关系详情")
		public Result<UserPermission> detail(@ApiParam(value = "用户权限关系id", required = true) @PathVariable("id") long id) {
        return Result.success(userPermissionService.getById(id));
    }

    /**
     * 添加或者更新用户权限关系
     * 
     * @param userPermission
     *            用户权限关系数据
     * @return 用户权限关系
     */
    @PutMapping("user-permission")
     @ApiOperation("增加/编辑用户权限关系")
      public Result<UserPermission> saveOrUpdate(@ApiParam("增加/编辑用户权限关系选项")@RequestBody UserPermission userPermission) {
        if (userPermission.getId() > 0) {
            return userPermissionService.update(userPermission, Wrappers.<UserPermission>lambdaUpdate().eq(UserPermission::getId, userPermission.getId())) ? Result.success(userPermission) : Result.fail("更新用户权限关系分组失败!");
        }
        return userPermissionService.save(userPermission) ? Result.success(userPermission) : Result.fail("保存用户权限关系失败!");
    }

    /**
     * 删除用户权限关系
     * 
     * @param id
     *            用户权限关系id
     * @return 是否删除成功
     */
    @DeleteMapping("user-permission/{id}")
     @ApiOperation("删除用户权限关系")
      public Result<Void> delete(@ApiParam(value = "用户权限关系id", required = true)@PathVariable("id") long id) {
        return userPermissionService.removeById(id) ? Result.success() : Result.fail("删除用户权限关系失败!");
    }

}
