package cn.com.chinarecrm.template.entity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

/**
 * <p>
 * 用户角色关系
 * </p>
 *
 * @author chinarecrm
 * @since 2021-01-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@TableName("t_user_role")
@ApiModel(value="UserRole", description="用户角色关系")
public class UserRole extends BaseEntity{


    @ApiModelProperty(value = "角色 id")
    @TableField("ur_role_id")
    private Long roleId;

    @ApiModelProperty(value = "用户 id")
    @TableField("ur_user_id")
    private Long userId;

}