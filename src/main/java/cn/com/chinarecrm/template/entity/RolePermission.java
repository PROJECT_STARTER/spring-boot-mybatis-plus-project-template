package cn.com.chinarecrm.template.entity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

/**
 * <p>
 * 角色权限关系表
 * </p>
 *
 * @author chinarecrm
 * @since 2021-01-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@TableName("t_role_permission")
@ApiModel(value="RolePermission", description="角色权限关系表")
public class RolePermission extends BaseEntity{


    @ApiModelProperty(value = "动作key")
    @TableField("rp_action_key")
    private String actionKey;

    @ApiModelProperty(value = "模块id")
    @TableField("rp_module_id")
    private Long moduleId;

    @ApiModelProperty(value = "模块key")
    @TableField("rp_module_key")
    private String moduleKey;

    @ApiModelProperty(value = "角色id")
    @TableField("rp_role_id")
    private Long roleId;

}