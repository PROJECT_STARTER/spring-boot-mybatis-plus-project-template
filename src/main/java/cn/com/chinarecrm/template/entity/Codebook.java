package cn.com.chinarecrm.template.entity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

/**
 * <p>
 * 码本数据
 * </p>
 *
 * @author chinarecrm
 * @since 2021-01-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@TableName("t_codebook")
@ApiModel(value="Codebook", description="码本数据")
public class Codebook extends BaseEntity{


    @ApiModelProperty(value = "分组Id")
    @TableField("c_group_id")
    private Long groupId;

    @ApiModelProperty(value = "上级Id")
    @TableField("c_parent_id")
    private Long parentId;

    @ApiModelProperty(value = "序号")
    @TableField("c_index")
    private Long index;

    @ApiModelProperty(value = "key")
    @TableField("c_key")
    private String key;

    @ApiModelProperty(value = "value")
    @TableField("c_value")
    private String value;

    @ApiModelProperty(value = "描述")
    @TableField("c_description")
    private String description;

    @ApiModelProperty(value = "禁用标识")
    @TableField("c_disabled")
    private Boolean disabled;

}