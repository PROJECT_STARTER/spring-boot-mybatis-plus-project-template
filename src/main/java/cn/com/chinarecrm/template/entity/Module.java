package cn.com.chinarecrm.template.entity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

/**
 * <p>
 * 功能模块
 * </p>
 *
 * @author chinarecrm
 * @since 2021-01-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@TableName("t_module")
@ApiModel(value="Module", description="功能模块")
public class Module extends BaseEntity{


    @ApiModelProperty(value = "模块key")
    @TableField("m_key")
    private String key;

    @ApiModelProperty(value = "模块描述")
    @TableField("m_descr")
    private String descr;

    @ApiModelProperty(value = "模块名称")
    @TableField("m_name")
    private String name;

}