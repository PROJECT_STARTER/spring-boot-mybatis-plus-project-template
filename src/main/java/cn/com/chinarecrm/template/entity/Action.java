package cn.com.chinarecrm.template.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

/**
 * <p>
 * 功能动作
 * </p>
 *
 * @author chinarecrm
 * @since 2021-01-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@TableName("t_action")
@ApiModel(value = "Action", description = "功能动作")
public class Action extends BaseEntity {

    @ApiModelProperty(value = "动作key")
    @TableField("a_key")
    private String key;

    @ApiModelProperty(value = "是否内置标识")
    @TableField("a_installed")
    private Boolean installed;

    @ApiModelProperty(value = "归属的模块id")
    @TableField("a_module_id")
    private Long moduleId;

    @ApiModelProperty(value = "动作名称")
    @TableField("a_name")
    private String name;

}