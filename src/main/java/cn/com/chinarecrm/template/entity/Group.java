package cn.com.chinarecrm.template.entity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

/**
 * <p>
 * 码本分组
 * </p>
 *
 * @author chinarecrm
 * @since 2021-01-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@TableName("t_group")
@ApiModel(value="Group", description="码本分组")
public class Group extends BaseEntity{


    @ApiModelProperty(value = "分组唯一键")
    @TableField("g_key")
    private String key;

    @ApiModelProperty(value = "分组名称")
    @TableField("g_name")
    private String name;

    @ApiModelProperty(value = "分组描述")
    @TableField("g_descr")
    private String descr;

    @ApiModelProperty(value = "禁用标识")
    @TableField("g_disabled")
    private Boolean disabled;

}