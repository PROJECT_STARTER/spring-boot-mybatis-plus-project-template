package cn.com.chinarecrm.template.entity;

import java.time.LocalDateTime;

import org.nutz.lang.Lang;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.core.JsonProcessingException;

import cn.com.chinarecrm.template.utils.Beans;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder.Default;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(of = "id")
public class BaseEntity {

    @TableId(value = "id", type = IdType.AUTO)
    protected Long id;

    @TableField(value = "created_time", fill = FieldFill.INSERT)
    @Default
    protected LocalDateTime createTime = LocalDateTime.now();

    @TableField(value = "updated_time", fill = FieldFill.UPDATE)
    @Default
    protected LocalDateTime updateTime = LocalDateTime.now();

    @ApiModelProperty(value = "创建人")
    @TableField("created_by")
    protected String createdBy;

    @ApiModelProperty(value = "更新人")
    @TableField("updated_by")
    protected String updatedBy;

    /**
     * 转换对象
     * 
     * @param <T>
     * @param clazz
     * @return
     */
    public <T extends BaseEntity> T exchange(Class<T> clazz) {
        try {
            return Beans.deepCopy(this, clazz);
        } catch (JsonProcessingException e) {
            throw Lang.wrapThrow(e);
        }
    }

}
