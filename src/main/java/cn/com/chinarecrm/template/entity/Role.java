package cn.com.chinarecrm.template.entity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

/**
 * <p>
 * 角色
 * </p>
 *
 * @author chinarecrm
 * @since 2021-01-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@TableName("t_role")
@ApiModel(value="Role", description="角色")
public class Role extends BaseEntity{


    @ApiModelProperty(value = "角色唯一键")
    @TableField("r_key")
    private String key;

    @ApiModelProperty(value = "角色名称")
    @TableField("r_name")
    private String name;

    @ApiModelProperty(value = "角色描述")
    @TableField("r_descr")
    private String descr;

}