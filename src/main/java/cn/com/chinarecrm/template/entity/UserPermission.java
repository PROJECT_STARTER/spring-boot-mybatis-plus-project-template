package cn.com.chinarecrm.template.entity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

/**
 * <p>
 * 用户权限关系
 * </p>
 *
 * @author chinarecrm
 * @since 2021-01-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@TableName("t_user_permission")
@ApiModel(value="UserPermission", description="用户权限关系")
public class UserPermission extends BaseEntity{


    @ApiModelProperty(value = "动作key")
    @TableField("up_action_key")
    private String actionKey;

    @ApiModelProperty(value = "模块id")
    @TableField("up_module_id")
    private Long moduleId;

    @ApiModelProperty(value = "模块key")
    @TableField("up_module_key")
    private String moduleKey;

    @ApiModelProperty(value = "用户id")
    @TableField("up_user_id")
    private Long userId;

}