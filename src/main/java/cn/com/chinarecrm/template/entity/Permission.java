package cn.com.chinarecrm.template.entity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

/**
 * <p>
 * 权限
 * </p>
 *
 * @author chinarecrm
 * @since 2021-01-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@TableName("t_permission")
@ApiModel(value="Permission", description="权限")
public class Permission extends BaseEntity{


    @ApiModelProperty(value = "权限唯一键")
    @TableField("p_key")
    private String key;

    @ApiModelProperty(value = "权限名称")
    @TableField("p_name")
    private String name;

    @ApiModelProperty(value = "权限描述")
    @TableField("p_descr")
    private String descr;

}