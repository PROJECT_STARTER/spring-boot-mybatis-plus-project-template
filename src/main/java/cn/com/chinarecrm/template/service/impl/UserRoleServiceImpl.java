package cn.com.chinarecrm.template.service.impl;

import cn.com.chinarecrm.template.entity.UserRole;
import cn.com.chinarecrm.template.mapper.UserRoleMapper;
import cn.com.chinarecrm.template.service.UserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色关系 服务实现类
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

}
