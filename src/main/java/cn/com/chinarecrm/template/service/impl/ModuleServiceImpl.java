package cn.com.chinarecrm.template.service.impl;

import cn.com.chinarecrm.template.entity.Module;
import cn.com.chinarecrm.template.mapper.ModuleMapper;
import cn.com.chinarecrm.template.service.ModuleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 功能模块 服务实现类
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
@Service
public class ModuleServiceImpl extends ServiceImpl<ModuleMapper, Module> implements ModuleService {

}
