package cn.com.chinarecrm.template.service.impl;

import cn.com.chinarecrm.template.entity.RolePermission;
import cn.com.chinarecrm.template.mapper.RolePermissionMapper;
import cn.com.chinarecrm.template.service.RolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色权限关系表 服务实现类
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {

}
