package cn.com.chinarecrm.template.service.impl;

import cn.com.chinarecrm.template.entity.Role;
import cn.com.chinarecrm.template.mapper.RoleMapper;
import cn.com.chinarecrm.template.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色 服务实现类
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

}
