package cn.com.chinarecrm.template.service.impl;

import cn.com.chinarecrm.template.entity.Group;
import cn.com.chinarecrm.template.mapper.GroupMapper;
import cn.com.chinarecrm.template.service.GroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 码本分组 服务实现类
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
@Service
public class GroupServiceImpl extends ServiceImpl<GroupMapper, Group> implements GroupService {

}
