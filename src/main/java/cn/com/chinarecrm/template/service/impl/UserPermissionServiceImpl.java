package cn.com.chinarecrm.template.service.impl;

import cn.com.chinarecrm.template.entity.UserPermission;
import cn.com.chinarecrm.template.mapper.UserPermissionMapper;
import cn.com.chinarecrm.template.service.UserPermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户权限关系 服务实现类
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
@Service
public class UserPermissionServiceImpl extends ServiceImpl<UserPermissionMapper, UserPermission> implements UserPermissionService {

}
