package cn.com.chinarecrm.template.service.impl;

import cn.com.chinarecrm.template.entity.Action;
import cn.com.chinarecrm.template.mapper.ActionMapper;
import cn.com.chinarecrm.template.service.ActionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 功能动作 服务实现类
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
@Service
public class ActionServiceImpl extends ServiceImpl<ActionMapper, Action> implements ActionService {

}
