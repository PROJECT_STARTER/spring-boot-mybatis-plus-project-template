package cn.com.chinarecrm.template.service;

import cn.com.chinarecrm.template.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色 服务类
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
public interface RoleService extends IService<Role> {

}
