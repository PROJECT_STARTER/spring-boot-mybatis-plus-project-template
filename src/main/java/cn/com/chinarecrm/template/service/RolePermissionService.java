package cn.com.chinarecrm.template.service;

import cn.com.chinarecrm.template.entity.RolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色权限关系表 服务类
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
public interface RolePermissionService extends IService<RolePermission> {

}
