package cn.com.chinarecrm.template.service;

import cn.com.chinarecrm.template.entity.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户角色关系 服务类
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
public interface UserRoleService extends IService<UserRole> {

}
