package cn.com.chinarecrm.template.service.impl;

import cn.com.chinarecrm.template.entity.User;
import cn.com.chinarecrm.template.mapper.UserMapper;
import cn.com.chinarecrm.template.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户 服务实现类
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
