package cn.com.chinarecrm.template.service;

import cn.com.chinarecrm.template.entity.Group;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 码本分组 服务类
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
public interface GroupService extends IService<Group> {

}
