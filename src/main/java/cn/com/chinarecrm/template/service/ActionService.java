package cn.com.chinarecrm.template.service;

import cn.com.chinarecrm.template.entity.Action;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 功能动作 服务类
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
public interface ActionService extends IService<Action> {

}
