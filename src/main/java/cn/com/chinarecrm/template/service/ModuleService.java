package cn.com.chinarecrm.template.service;

import cn.com.chinarecrm.template.entity.Module;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 功能模块 服务类
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
public interface ModuleService extends IService<Module> {

}
