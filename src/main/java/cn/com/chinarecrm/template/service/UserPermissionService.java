package cn.com.chinarecrm.template.service;

import cn.com.chinarecrm.template.entity.UserPermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户权限关系 服务类
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
public interface UserPermissionService extends IService<UserPermission> {

}
