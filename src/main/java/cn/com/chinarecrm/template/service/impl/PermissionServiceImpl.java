package cn.com.chinarecrm.template.service.impl;

import cn.com.chinarecrm.template.entity.Permission;
import cn.com.chinarecrm.template.mapper.PermissionMapper;
import cn.com.chinarecrm.template.service.PermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 权限 服务实现类
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {

}
