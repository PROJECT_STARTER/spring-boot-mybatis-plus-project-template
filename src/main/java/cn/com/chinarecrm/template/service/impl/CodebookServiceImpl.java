package cn.com.chinarecrm.template.service.impl;

import cn.com.chinarecrm.template.entity.Codebook;
import cn.com.chinarecrm.template.mapper.CodebookMapper;
import cn.com.chinarecrm.template.service.CodebookService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 码本数据 服务实现类
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
@Service
public class CodebookServiceImpl extends ServiceImpl<CodebookMapper, Codebook> implements CodebookService {

}
