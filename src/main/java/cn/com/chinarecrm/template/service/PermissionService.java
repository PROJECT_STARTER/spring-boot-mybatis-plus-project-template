package cn.com.chinarecrm.template.service;

import cn.com.chinarecrm.template.entity.Permission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 权限 服务类
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
public interface PermissionService extends IService<Permission> {

}
