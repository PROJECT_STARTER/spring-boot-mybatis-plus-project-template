package cn.com.chinarecrm.template.service;

import cn.com.chinarecrm.template.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户 服务类
 * </p>
 *
 * @author chinarecrm
 * @since 2020-12-19
 */
public interface UserService extends IService<User> {

}
