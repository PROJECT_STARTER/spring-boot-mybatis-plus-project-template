package cn.com.chinarecrm.template.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CommonStatus implements BaseEnum {
    /**
     * 
     */
    CREATE("新建"),
    /**
     * 
     */
    ENABLED("启用"),
    /**
     * 
     */
    DISABLED("停用"),
    /**
     * 
     */
    DELETE("删除");

    private String name;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getCode() {
        return this.name();
    }

}