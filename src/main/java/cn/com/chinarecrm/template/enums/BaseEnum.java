package cn.com.chinarecrm.template.enums;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import cn.com.chinarecrm.template.config.jackson.BaseEnumDeserializer;
import cn.com.chinarecrm.template.config.jackson.BaseEnumSerializer;

@JsonDeserialize(using = BaseEnumDeserializer.class)
@JsonSerialize(using = BaseEnumSerializer.class)
public interface BaseEnum {

    String getCode();

    String getName();

    static <E extends Enum<E>> BaseEnum valueOf(String enumCode, Class<E> clazz) {
        return (BaseEnum)Enum.valueOf(clazz, enumCode);
    }
}
